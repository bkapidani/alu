ALU

Operazioni necessare:

somma
comparazione bit a bit
NAND bit a bit
passare in uscita l'ingresso 1 inalterato


Componenti necessari:

sommatore (16bit) (usato come modulo esterno)
comparatore (16bit)
NAND bitwise
mux 3 ingressi (in1, add_out, nand_out)


Interfaccia:

2 ingressi dati a 16bit
1 ingresso controllo
1 uscita dati a 16 bit
1 uscita a 1 bit per EQ


