#include <systemc.h>
#include <iostream>
#include <string>
#include "alu.hpp"

typedef sc_signal<sc_bv<16> >         channel16;
typedef sc_signal<sc_bv<2> >          channel2;
typedef sc_signal<sc_bv<1> >          channel1;

int sc_main (int argc, char *argv[])
{
   bool error = false;

   channel16 in1("in1");
   channel16 in2("in2");
   channel2 controllo("controllo");
   channel16 uscita("uscita");
   channel1 uscita_eq("uscita_eq");

   in1.write("0000000000000001");
   in2.write("0000000000000010");
   controllo.write("00");

   Alu myAlu("alu");
   myAlu.src1(in1);
   myAlu.src2(in2);
   myAlu.control(controllo);
   myAlu.result(uscita);
   myAlu.equality(uscita_eq);

   sc_start();

   error |= ((uscita.read()) != in1.read());

   return (error ? -1 : 0);
}

