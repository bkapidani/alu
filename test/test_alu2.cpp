#include "systemc.h"
#include <iostream>
#include "alu_stimulus.hpp"
#include "alu.hpp"

int sc_main (int argc, char* argv[])
{
   sc_clock clock("clock");     
   sc_signal<sc_bv<16> > src1;
   sc_signal<sc_bv<16> > src2;
   sc_signal<sc_bv<2> > control;
   sc_signal<sc_bv<16> > result;
   sc_signal<sc_bv<1> > equality;

   Alu_Stimulus s1("Alu_Stimulus");
     
   s1.src1(src1);
   s1.src2(src2);
   s1.control(control);
   s1.result(result);
   s1.equality(equality);
   s1.clock(clock);

   Alu alu("Alu");
        
   alu.src1(src1);
   alu.src2(src2);
   alu.control(control);
   alu.result(result);
   alu.equality(equality);

   //Open VCD file
   sc_trace_file *wf = sc_create_vcd_trace_file("test_alu2_vcd_tracingfile");

   //Dump the desired signals
   sc_trace(wf, src1, "src1");
   sc_trace(wf, src2, "src2");
   sc_trace(wf, control, "control");
   sc_trace(wf, result, "result");
   sc_trace(wf, equality, "equality");

   sc_start(SC_ZERO_TIME);
   sc_start(100, SC_NS);

   sc_close_vcd_trace_file(wf);

   return 0;
}


