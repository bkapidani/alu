// alu

#include <systemc.h>
#include "alu.hpp"

using namespace std;

SC_HAS_PROCESS(Alu);

Alu::Alu(sc_module_name name) :
   sc_module(name), mux1("mux"), nand1("nand"), comparator1("comparator"), adder1("adder")
{
   mux1.in1(this->src1);
   mux1.in2(this->adder_out_ch);
   mux1.in3(this->nand_out_ch);
   mux1.sel(this->control);
   mux1.result(this->result);
   nand1.A(this->src1);
   nand1.B(this->src2);
   nand1.S(this->nand_out_ch);
   comparator1.A(this->src1);
   comparator1.B(this->src2);
   comparator1.S(this->equality);
   adder1.a(this->src1);
   adder1.b(this->src2);
   adder1.s(this->adder_out_ch);
}

