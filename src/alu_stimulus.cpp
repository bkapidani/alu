#include "alu_stimulus.hpp"


void Alu_Stimulus::stimgen()
{
   while(true)
   {                                 
      src1 = "0101010101010101";
      src2 = "1010101010101010";
      control = "00";
      wait();

      wait();
      
      cout << "src1 = " << src1 << endl;
      cout << "src2 = " << src2 << endl;
      cout << "control = " << control << endl;
      cout << "equality = " << equality << endl;
      cout << "result = " << result << endl << endl << endl;

      src1 = "0101010101010101";
      src2 = "1010101010101010";
      control = "01";

      wait();


      cout << "src1 = " << src1 << endl;
      cout << "src2 = " << src2 << endl;
      cout << "control = " << control << endl;
      cout << "equality = " << equality << endl;
      cout << "result = " << result << endl << endl << endl;

      src1 = "0101010101010101";
      src2 = "1010101010101010";
      control = "10";

      wait();

      cout << "src1 = " << src1 << endl;
      cout << "src2 = " << src2 << endl;
      cout << "control = " << control << endl;
      cout << "equality = " << equality << endl;
      cout << "result = " << result << endl << endl << endl;

      src1 = "0000000000000001";
      src2 = "0000000000000001";
      control = "01";

      wait();

      cout << "src1 = " << src1 << endl;
      cout << "src2 = " << src2 << endl;
      cout << "control = " << control << endl;
      cout << "equality = " << equality << endl;
      cout << "result = " << result << endl << endl << endl;

      src1 = "0000000000000001";
      src2 = "0000000000000001";
      control = "10";

      wait();

      cout << "src1 = " << src1 << endl;
      cout << "src2 = " << src2 << endl;
      cout << "control = " << control << endl;
      cout << "equality = " << equality << endl;
      cout << "result = " << result << endl << endl << endl;

      src1 = "0000000000000001";
      src2 = "0000000000000001";
      control = "00";

      wait();

      cout << "src1 = " << src1 << endl;
      cout << "src2 = " << src2 << endl;
      cout << "control = " << control << endl;
      cout << "equality = " << equality << endl;
      cout << "result = " << result << endl << endl << endl;
                   
      return;
   }
}

