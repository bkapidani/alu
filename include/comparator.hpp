#ifndef COMPARATOR_HPP
#define COMPARATOR_HPP

#include <systemc.h>

using namespace std;

SC_MODULE(Comparator)
{
   sc_in<sc_bv<16> > A;
   sc_in<sc_bv<16> > B;
   sc_out<sc_bv<1> > S;

   void do_compare()
   {
      S = (~(A->read() ^ B->read())).and_reduce();
   }

   SC_CTOR(Comparator) {
      SC_METHOD(do_compare); 
      sensitive << A << B; 
   }

};

#endif
