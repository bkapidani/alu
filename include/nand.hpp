#ifndef NAND_HPP
#define NAND_HPP

#include <systemc.h>

using namespace std;

SC_MODULE(Nand)
{
   sc_in<sc_bv<16> > A;
   sc_in<sc_bv<16> > B;
   sc_out<sc_bv<16> > S;

   void do_nand()
   {
      S = ~( A->read() & B->read() );
   }

   SC_CTOR(Nand) {
      SC_METHOD(do_nand); 
      sensitive << A << B; 
   }

};

#endif
