#ifndef ALU_STIMULUS_HPP
#define ALU_STIMULUS_HPP

#include <systemc.h>

SC_MODULE(Alu_Stimulus){

        //Declaration of the inputs
        sc_in<bool> clock;
        sc_in<sc_bv<16> > result;
        sc_in<sc_bv<1> > equality;

        //Declaration of the outputs
        sc_out<sc_bv<16> > src1;
        sc_out<sc_bv<16> > src2;
        sc_out<sc_bv<2> > control;


        void stimgen();

        SC_CTOR(Alu_Stimulus){

                SC_THREAD(stimgen);
                sensitive_pos << clock; 

        }

};

#endif

