// ALU:

// OPERATION SELECTION:
// 		sel = "00" selects data1
// 		sel = "01" selects adder_out
// 		sel = "1-" selects nand_out

#ifndef ALU_HPP
#define ALU_HPP

#include <systemc.h>
#include "nand.hpp"
#include "comparator.hpp"
#include "multiplexer_3x16.hpp"
#include "adder.hpp"

using namespace std;

SC_MODULE(Alu)
{
   sc_in<sc_bv<16> > src1;
   sc_in<sc_bv<16> > src2;
   sc_in<sc_bv<2> > control;
   sc_out<sc_bv<16> > result;
   sc_out<sc_bv<1> > equality;

   sc_signal<sc_bv<16> > adder_out_ch;
   sc_signal<sc_bv<16> > nand_out_ch;

   Multiplexer_3x16 mux1;
   Nand nand1;
   Comparator comparator1;
   Adder adder1;

   Alu(sc_module_name name);

};



#endif
